/*
Copyright 2020 Micronix Solutions, LLC (https://micronix.solutions/).
Released under the terms of the Modified BSD License
https://opensource.org/licenses/BSD-3-Clause
*/

package sqlx

import (
	"database/sql"
	"errors"
	"fmt"
	"reflect"
	"sort"
	"strconv"
	"strings"
)

type saveType int

const (
	saveTypeInsert saveType = iota
	saveTypeUpdate saveType = iota
)

type structDBField struct {
	dbName string
	index  int
	isPK   bool
}

type saveStatementSet struct {
	insertStatement string
	updateStatement string
}

type structDBFieldMap map[string]structDBField

func Ping(db *sql.DB) (err error) {
	rows, err := db.Query("SELECT 1;")
	rows.Close()
	return
}

func Save(s interface{}, db *sql.DB, table string) (sql.Result, error) {
	if reflect.TypeOf(s).Kind() != reflect.Ptr {
		return nil, errors.New("destination must be a pointer to a struct")
	}

	var structVal = reflect.Indirect(reflect.ValueOf(s))

	var structType = structVal.Type()
	if structType.Kind() != reflect.Struct {
		return nil, errors.New("destination must be a pointer to a struct")
	}

	fields, err := dbFieldsForStruct(structType)
	if err != nil {
		return nil, err
	}

	stmts, err := saveStatementsForStruct(structType, table)
	if err != nil {
		return nil, err
	}

	var st = saveTypeUpdate
	var pkField string
	var sv = make([]interface{}, 0, len(fields))

	var mkeys = make([]string, 0)
	for k, _ := range fields {
		mkeys = append(mkeys, k)
	}
	sort.Strings(mkeys)

	for _, vkey := range mkeys {

		if fields[vkey].isPK {

			pkField = fields[vkey].dbName
			if structVal.FieldByIndex([]int{fields[vkey].index}).IsZero() {
				st = saveTypeInsert
			}

		} else {

			sv = append(sv, structVal.FieldByIndex([]int{fields[vkey].index}).Interface())

		}
	}

	if pkField == "" {
		return nil, fmt.Errorf("Missing PK field for struct %s", structType.Name())
	}

	var stmt *sql.Stmt

	switch st {

	case saveTypeInsert:
		stmt, err = db.Prepare(stmts.insertStatement)
		fmt.Println(stmts.insertStatement)
		if err != nil {
			return nil, err
		}

	case saveTypeUpdate:
		sv = append(sv, structVal.FieldByIndex([]int{fields[pkField].index}).Interface())

		stmt, err = db.Prepare(stmts.updateStatement)
		fmt.Println(stmts.updateStatement)
		if err != nil {
			return nil, err
		}
	}

	return stmt.Exec(sv...)
}

func Get(destination interface{}, db *sql.DB, query string, args ...interface{}) error {
	stmt, err := db.Prepare(query)
	if err != nil {
		return err
	}

	rows, err := stmt.Query(args...)
	if err != nil {
		return err
	}

	return Unmarshal(rows, destination)
}

func GetSingle(destination interface{}, db *sql.DB, query string, args ...interface{}) error {
	stmt, err := db.Prepare(query)
	if err != nil {
		return err
	}

	rows, err := stmt.Query(args...)
	if err != nil {
		return err
	}

	return UnmarshalSingle(rows, destination)
}

func UnmarshalSingle(rows *sql.Rows, destination interface{}) error {

	if reflect.TypeOf(destination).Kind() != reflect.Ptr {
		return errors.New("destination must be a pointer to a struct")
	}

	var structType = reflect.Indirect(reflect.ValueOf(destination)).Type()
	if structType.Kind() != reflect.Struct {
		return errors.New("destination must be a pointer to a struct")
	}

	var structVal = reflect.ValueOf(destination)
	var slice = reflect.MakeSlice(reflect.SliceOf(structType), 0, 1)

	var pointer = reflect.New(slice.Type())
	pointer.Elem().Set(slice)

	if err := Unmarshal(rows, pointer.Interface()); err != nil {
		return err
	}

	if reflect.Indirect(pointer).Len() > 0 {
		reflect.Indirect(structVal).Set(reflect.Indirect(pointer).Index(0))
	}

	return nil
}

func Unmarshal(rows *sql.Rows, destination interface{}) error {
	if reflect.TypeOf(destination).Kind() != reflect.Ptr {
		return errors.New("destination must be a *[]<SomeStruct> (0 length is OK)")
	}

	var destSlice = reflect.Indirect(reflect.ValueOf(destination))
	if destSlice.Kind() != reflect.Slice {
		return errors.New("destination must be a *[]<SomeStruct> (0 length is OK)")
	}

	var structType = destSlice.Type().Elem()
	if structType.Kind() != reflect.Struct {
		return errors.New("destination must be a *[]<SomeStruct> (0 length is OK)")
	}

	structFields, err := dbFieldsForStruct(structType)
	if err != nil {
		return err
	}

	dbColumns, err := rows.Columns()
	if err != nil {
		return err
	}

	orderedFields, err := mapStructFieldsToQueryColumns(structFields, dbColumns)
	if err != nil {
		return err
	}

	for rows.Next() {
		var rowStruct = reflect.New(structType)
		var rowStructFields = make([]interface{}, len(orderedFields))
		for idx, fieldIndex := range orderedFields {
			rowStructFields[idx] = reflect.Indirect(rowStruct).Field(fieldIndex).Addr().Interface()
		}

		if err := rows.Scan(rowStructFields...); err != nil {
			return err
		}

		destSlice.Set(reflect.Append(destSlice, reflect.Indirect(rowStruct)))
	}

	return nil
}

var structSaveStatementCache map[reflect.Type]saveStatementSet

func saveStatementsForStruct(st reflect.Type, table string) (saveStatementSet, error) {
	if structSaveStatementCache == nil {
		structSaveStatementCache = make(map[reflect.Type]saveStatementSet)
	}

	if val, ok := structSaveStatementCache[st]; ok {
		return val, nil
	}

	sf, err := dbFieldsForStruct(st)
	if err != nil {
		return saveStatementSet{}, err
	}

	var insertFieldListBuilder strings.Builder
	var insertValueListBuilder strings.Builder
	var updateFieldValueBuilder strings.Builder
	var updateWhereClauseBuilder strings.Builder

	var mkeys = make([]string, 0)
	for k, _ := range sf {
		mkeys = append(mkeys, k)
	}
	sort.Strings(mkeys)

	for _, fld := range mkeys {
		if !sf[fld].isPK {
			insertFieldListBuilder.WriteString(fmt.Sprintf("%s, ", sf[fld].dbName))
			insertValueListBuilder.WriteString("?, ")
		}

		switch sf[fld].isPK {
		case true:
			updateWhereClauseBuilder.WriteString(fmt.Sprintf("%s = ?", sf[fld].dbName))
		case false:
			updateFieldValueBuilder.WriteString(fmt.Sprintf("%s = ?, ", sf[fld].dbName))
		}
	}

	var insertFields = strings.TrimSuffix(insertFieldListBuilder.String(), ", ")
	var insertValues = strings.TrimSuffix(insertValueListBuilder.String(), ", ")
	var insertStatement = fmt.Sprintf("INSERT INTO %s (%s) VALUES(%s);", table, insertFields, insertValues)

	var updateFieldValues = strings.TrimSuffix(updateFieldValueBuilder.String(), ", ")
	var updateStatement = fmt.Sprintf("UPDATE %s SET %s WHERE %s;", table, updateFieldValues, updateWhereClauseBuilder.String())

	structSaveStatementCache[st] = saveStatementSet{
		insertStatement: insertStatement,
		updateStatement: updateStatement,
	}

	return structSaveStatementCache[st], nil
}

var structDbFieldsCache map[reflect.Type]structDBFieldMap

func dbFieldsForStruct(st reflect.Type) (structDBFieldMap, error) {
	if structDbFieldsCache == nil {
		structDbFieldsCache = make(map[reflect.Type]structDBFieldMap)
	}

	if val, ok := structDbFieldsCache[st]; ok {
		return val, nil
	}

	var structFields = make(map[string]structDBField)

	var fieldName string

	for i := 0; i < st.NumField(); i++ {
		var field = st.Field(i)
		var pk bool

		if val, ok := field.Tag.Lookup("db"); ok {
			fieldName = val
		} else {
			continue
		}

		if _, ok := structFields[fieldName]; ok {
			return nil, errors.New("Duplicate field name: " + fieldName)
		}

		if val, ok := field.Tag.Lookup("pk"); ok {

			var err error
			pk, err = strconv.ParseBool(val)
			if err != nil {
				return nil, fmt.Errorf("Invalid value %s for field %s", val, fieldName)
			}

		}

		structFields[fieldName] = structDBField{
			dbName: fieldName,
			index:  i,
			isPK:   pk,
		}
	}

	structDbFieldsCache[st] = structFields

	return structFields, nil
}

func mapStructFieldsToQueryColumns(sfm structDBFieldMap, qc []string) ([]int, error) {
	var orderedFields = make([]int, 0)

	for _, col := range qc {
		if sf, ok := sfm[col]; ok {
			orderedFields = append(orderedFields, sf.index)
		} else {
			return nil, errors.New("No destination found in struct for DB field " + col)
		}
	}

	return orderedFields, nil
}

type NamedQueryCollection map[string]string
type NamedQueriesAggregation map[string]NamedQueryCollection

type MySQLConfig struct {
	Server   string
	Username string
	Password string
	Database string
	Queries  NamedQueriesAggregation
}

func (me *MySQLConfig) MakeConnectionString() string {
	return fmt.Sprintf("%s:%s@tcp(%s)/%s?parseTime=true", me.Username, me.Password, me.Server, me.Database)
}
